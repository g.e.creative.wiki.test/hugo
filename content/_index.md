## Front Page Content

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.

## 目次

<!-- TOC -->

- 目次
- このドキュメントについて
    - 本書の目的
    - 開発環境
- 命名規則
    - サンプルコード
    - 共通スタイルの説明
    - 二文字の名前(※)
    - 名前空間
    - アセンブリ
    - リソース
    - ファイル
    - クラス
    - フィールド(※変更)
    - 静的フィールド(≒グローバル変数)
    - コントロールのフィールド(※変更)
    - プロパティ
    - メソッド(≒関数)
    - 非同期メソッド(※追加)
    - パラメータ(≒引数)
    - ローカル変数、ループ変数
    - コンパイル時定数、実行時定数
    - 抽象クラス
    - インターフェイス
    - デリゲート(≒関数ポインタ)
    - イベント
    - 構造体
    - 列挙体
- コーディング規則
    - レイアウト規則
    - コメント規則
    - 長い名前
    - 多い引数
    - 多い演算子
    - プロパティ(※変更)
    - イベント処理(※変更)
- イディオム
    - タブとスペースではスペースを使う
    - ifの中括弧の省略はしない
    - 否定形の名前は避ける
    - マジックナンバーは使わない
    - 変数は一度だけ設定する
    - i++ と ++i
    - for の比較演算子
    - 比較演算子の向き
    - ループよりLINQ(クエリ式)を使う
    - キャストは as を使う
    - 変わった書き方は避ける
    - ガード節は積極的に使う
    - private なフィールドには _-(アンダーバー) を付ける
    - this. および ClassName. の推奨
    - var の使いどころ
    - const と readonly の使い分け
    - メソッドとプロパティの使い分け
    - 解放が必要なオブジェクトには using を使う
    - Dispose() 後は null を設定する
    - Array, ArrayList, List<T> の使い分け
    - Hashtable, Dictionary<TKey, TValue> の使い分け
    - 一定時間スリープさせる方法
    - アクセス修飾子について
    - 修飾子の順番
    - interface と abstract class の使い分け
    - unsigned 型はなるべく使わない
    - 拡張メソッドについて
    - コードの依存関係
    - ユーティリティクラスの是非
- コメント
    - コメント タグ
    - インテリセンスの表示例
- 付録
    - 逐語的リテラル文字列
    - 複合書式指定
    - プログラミング原則、格言
    - 拡張機能および外部ツール
    - 参考資料